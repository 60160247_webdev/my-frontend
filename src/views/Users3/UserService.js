const userService = {
  userList: [
    {
      id: 1,
      name: 'Thanakrit',
      surname: 'Wutthiphithak',
      gender: 'ผู้ชาย',
      birthday: '12 มีนาคม 2542',
      education: 'ปริญญาตรี',
      section: 'แผนกไอที',
      salary: 20000
    },
    {
      id: 2,
      name: 'Yanisa',
      surname: 'Tongtavee',
      gender: 'ผู้หญิง',
      birthday: '27 สิงหาคม 2542',
      education: 'ปริญญาตรี',
      section: 'แผนกการตลาด',
      salary: 30000
    }
  ],
  lastId: 3,
  addUser (user) {
    user.id = this.lastId++
    this.userList.push(user)
  },
  updateUser (user) {
    const index = this.userList.findIndex(item => item.id === user.id)
    this.userList.splice(index, 1, user)
  },
  deleteUser (user) {
    const index = this.userList.findIndex(item => item.id === user.id)
    this.userList.splice(index, 1)
  },
  getUsers () {
    return [...this.userList]
  }
}

export default userService
